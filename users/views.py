import csv
import json

from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render
from django.template import loader
from django.template.defaulttags import url
from django.urls import reverse
from django.views.decorators.csrf import csrf_exempt

import logging

from django.views.generic import ListView

from users.forms import ManualLogForm
from users.models import Card, DoorLog, Person

from datetime import datetime

logger = logging.getLogger(__name__)

LOG_ACTION = "LOG_ACTION"


def index(request):
    cards = Card.objects.filter(person_id=None).all()
    respose = []
    for card in cards:
        respose.append(
            dict(
                uid=card.uid,
                last_used=card.doorlog_set.latest("event_date").event_date,
                card_id=card.id,
            )
        )
    template = loader.get_template("unclaimed_cards_list.html")
    context = {
        "cards": respose,
    }
    return HttpResponse(template.render(context, request))


class LogListView(ListView):
    model = DoorLog
    ordering = ["-event_date"]

    def head(self, *args, **kwargs):
        last_log = self.get_queryset().latest("event_date")
        response = HttpResponse(
            # RFC 1123 date format.
            headers={
                "Last-Modified": last_log.added.strftime("%a, %d %b %Y %H:%M:%S GMT")
            },
        )
        return response

    def get_context_data(self, **kwargs):
        ctx = super(LogListView, self).get_context_data(**kwargs)
        ctx["fields"] = ["Aeg", "Isik"]
        return ctx


def add_manual_log_entry(request):
    # if this is a POST request we need to process the form data
    if request.method == "POST":
        # create a form instance and populate it with data from the request:
        form = ManualLogForm(request.POST)
        # check whether it's valid:
        if form.is_valid():
            person = Person.objects.get_or_create(name=form.data.get("person_name"))[0]
            DoorLog.objects.create(
                person_id=person.id, src=form.data.get("src"), event_date=datetime.now()
            )
            return HttpResponseRedirect(reverse("log"))

    # if a GET (or any other method) we'll create a blank form
    else:
        form = ManualLogForm()

    return render(request, "manuallog.html", {"form": form})


@csrf_exempt
def new_action(request):
    if request.method == "POST":
        body = json.loads(request.body)
        saved_count = 0

        cards = body.get("cards")
        invalid_cards = []
        if cards:
            for card in cards:
                uid_hash = card.get("uid")
                event_date = card.get("eventDate")
                if uid_hash and event_date:
                    found_card = Card.objects.get_or_create(uid=uid_hash)[0]
                    doorlog = DoorLog.objects.create(
                        card=found_card,
                        person=found_card.person,
                        event_date=datetime.fromtimestamp(event_date),
                        src=LOG_ACTION,
                    )
                    doorlog.save()
                    saved_count += 1
                else:
                    invalid_cards.append(card)

        else:
            return HttpResponse("No cards given.")

        return HttpResponse(
            f"Ok. Added {saved_count} items. Invalid cards {len(invalid_cards)}"
        )

    return HttpResponse("Failure")


def export_to_csv(response):
    # Create the HttpResponse object with the appropriate CSV header.
    response = HttpResponse(
        content_type="text/csv",
        headers={"Content-Disposition": 'attachment; filename="noortekas.csv"'},
    )

    writer = csv.writer(response)
    writer.writerow(["Aeg", "Inimene", "Lisas"])
    items = DoorLog.objects.all().order_by("-event_date")
    for doorlog in items:
        event_date = doorlog.event_date.strftime('%Y-%m-%d %H:%M:%S')
        name = ""
        added_by = ""
        if doorlog.src == LOG_ACTION:
            if doorlog.card and doorlog.card.person:
                name = doorlog.card.person.name
            else:
                if doorlog.card:
                    name = doorlog.card.uid[:-10]
        else:
            name = doorlog.person.name
            added_by = doorlog.src
        writer.writerow((event_date, name, added_by))

    return response
