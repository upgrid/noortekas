# Generated by Django 4.0.1 on 2022-01-22 07:35

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0005_doorlog_eventdate'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='doorlog',
            name='uid',
        ),
    ]
