from django.core.validators import MaxValueValidator, MinValueValidator

from django.db import models


# Create your models here.
class Person(models.Model):
    name = models.CharField(db_index=True, max_length=50, null=True, blank=True,)
    active = models.BooleanField(db_index=True, default=True)
    added = models.DateTimeField(db_index=True, auto_now_add=True)
    updated = models.DateTimeField(db_index=True, auto_now=True)


    def __str__(self):
        return f"{self.id} | {self.name}"




class Card(models.Model):
    person = models.ForeignKey(
        Person,
        on_delete=models.CASCADE,
        null=True, blank=True,
        db_index=True
    )
    uid = models.CharField(db_index=True, max_length=41, unique=True)
    name = models.CharField(db_index=True, max_length=50, null=True, blank=True)
    active = models.BooleanField(db_index=True, default=True)
    added = models.DateTimeField(db_index=True, auto_now_add=True)
    updated = models.DateTimeField(db_index=True, auto_now=True)

    def __str__(self):
        return f" {self.uid} | {self.name}"






class DoorLog(models.Model):
    person = models.ForeignKey(
        Person,
        on_delete=models.CASCADE,
        null=True, blank=True,
        db_index=True
    )
    card = models.ForeignKey(
        Card,
        on_delete=models.CASCADE,
        null=True, blank=True,
        db_index=True
    )

    added = models.DateTimeField(db_index=True, auto_now_add=True)
    event_date = models.DateTimeField(db_index=True)
    src = models.CharField(db_index=True, max_length=50, null=True, blank=True)
