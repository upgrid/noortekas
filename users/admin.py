from django.contrib import admin

from .models import Card, Person, DoorLog


def activate_item(modeladmin, request, queryset):
    queryset.update(active=True)


def deactivate_item(modeladmin, request, queryset):
    queryset.update(active=False)


activate_item.short_description = "Activate selected items"
deactivate_item.short_description = "Deactivate selected items"




class TagInline(admin.TabularInline):
    """Inline configuration for Django's admin on the Tag model."""
    model = Card
    extra = 0
    readonly_fields = ("added", "updated", "uid")

    def get_extra(self, request, obj=None, **kwargs):
        """Dynamically sets the number of extra forms. 0 if the related object
        already exists or the extra configuration otherwise."""
        if obj:
            # Don't add any extra forms if the related object already exists.
            return 0
        return self.extra




class PersonAdmin(admin.ModelAdmin):
    list_display = ( "id", 'name', 'active',)
    search_fields = ("id", 'name', 'active')
    list_filter = ('active', )
    inlines = [TagInline]
    actions = (activate_item, deactivate_item)




class TagAdmin(admin.ModelAdmin):
    list_display = ("id", 'uid', 'name', "person", "active", "added", "updated")
    search_fields = ("id", 'uid', 'name', "person__name")
    list_filter = ("active", "added", "updated")
    readonly_fields = ("added", "updated", "uid")
    actions = (activate_item, deactivate_item)

class DoorLogAdmin(admin.ModelAdmin):
    search_fields = ("card__uid", "person__name", "event_date")
    list_display = ("id", "card", "person", "added", "event_date")
    readonly_fields = ("id", "card", "person", "added", "event_date")


admin.site.register(Person, PersonAdmin)
admin.site.register(Card, TagAdmin)
admin.site.register(DoorLog, DoorLogAdmin)
