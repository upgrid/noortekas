from django import forms

class ManualLogForm(forms.Form):
    person_name = forms.CharField(label='Registeeritav isik', max_length=30)
    src = forms.CharField(label='Lisaja', max_length=100)