from django.urls import path

from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('action/', views.new_action, name='action'),
    path('log/', views.LogListView.as_view(), name='log'),
    path('log/csv/', views.export_to_csv, name='exportcsv'),
    path('manuallog/', views.add_manual_log_entry, name='manuallog'),
]
