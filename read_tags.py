#! /usr/bin/env python3


from __future__ import print_function

import hashlib
from collections import deque
from datetime import datetime
from threading import Thread, Event
import time
import signal
import json

import requests
from smartcard.CardMonitoring import CardMonitor, CardObserver
from smartcard.util import toHexString, toBytes
import os.path

BASE_HOST = "http://localhost:8080"
ACTION_URL = f"{BASE_HOST}/users/action/"


# a simple card observer that tries to select DF_TELECOM on an inserted card
class selectDFTELECOMObserver(CardObserver):
    """A simple card observer that is notified
    when cards are inserted/removed from the system and
    prints the list of cards
    """

    def __init__(self, main):
        super().__init__()
        self.main = main

    @staticmethod
    def card_action(data_unit, card, decode=False):
        (response, sw1, sw2) = \
            card.connection.transmit(toBytes(data_unit))
        if sw1 == 97:  # Card sends selected bytestring lenght
            (response, sw1, sw2) = \
                card.connection.transmit(toBytes("00C00000" + toHexString([sw2])))
        if sw1 != 144:  # Not OK
            raise Exception("Response status is: %x %x used adpu: %s" % (sw1, sw2, data_unit))
        if decode:
            if response[0] in [0, 32]:  # Check if response is empty
                return ""
            return bytearray(response).decode(encoding='ISO 8859-1', errors="strict").strip()
        else:
            return response

    def update(self, observable, actions):
        (addedcards, removedcards) = actions
        for card in addedcards:
            if "Contactless" in card.reader or "ACS" in card.reader and "SAM" not in card.reader:
                card.connection = card.createConnection()
                card.connection.connect()
                uid = toHexString(self.card_action('FFCA000000', card))
                uid_hash = hashlib.sha224(uid.encode("utf-8")).hexdigest()
                print(f"Scan NFC Tag wit hash: {uid_hash}")
                self.main.new_tag_appeared(uid_hash)


class Main:
    def __init__(self):
        self.new_tags = deque([])
        self.cardmonitor = CardMonitor()
        self.selectobserver = selectDFTELECOMObserver(self)
        self.cardmonitor.addObserver(self.selectobserver)
        self.wait_event = Event()
        self.new_sleep = 10

        signal.signal(signal.SIGTERM, self.sigint_handler)
        signal.signal(signal.SIGINT, self.sigint_handler)

        t = Thread(target=self.clear_deque)
        t.daemon = True
        t.start()

    def clear_deque(self):
        while True:
            if self.wait_event.wait(timeout=self.new_sleep):
                self.wait_event.clear()

            contents = []
            while self.new_tags:
                contents.append(self.new_tags.popleft())
            if contents:
                resp = requests.post(ACTION_URL, json=dict(cards=contents))
                print(resp.text)

    # Handle ctrl-c
    def sigint_handler(self, signum, frame):
        self.cardmonitor.deleteObserver(self.selectobserver)
        print()
        exit()

    def new_tag_appeared(self, hash):
        self.new_tags.append(dict(uid=hash, eventDate=datetime.now().timestamp()))
        self.wait_event.set()


if __name__ == '__main__':

    print("Scan NFC Tag")
    print("This program will run forever or until you use Ctrl + C")
    print("")

    main = Main()

    while True:
        time.sleep(180)
